using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Deque;

public class Deque<T> : IEnumerable<T>
{
    private CircularList<T> _buffer;
    private int _version;
    public int Count { get { return _buffer.Count; } }

    public Deque() : this(32)
    {
    }

    public Deque(int initialCapacity)
    {
        _buffer = new CircularList<T>(initialCapacity);
        _version = 0;
    }

    public Deque(IEnumerable<T> collection)
    {
        int starting_size = collection.Count();
        _buffer = new CircularList<T>(starting_size * 2, collection);
        _version = 0;
    }

    public void Clear()
    {
        _buffer.Clear();
        ++_version;
    }

    public void CopyTo(T[] array, int index)
    {
        _buffer.CopyTo(array, index);
    }

    public void Enqueue(T item)
    {
        if (_buffer.IsFull)
        {
            Grow(_buffer.Count + 1);
        }
        ++_version;
        _buffer.PushRight(item);
    }

    public void EnqueueLeft(T item)
    {
        if (_buffer.IsFull)
        {
            Grow(_buffer.Count + 1);
        }
        ++_version;
        _buffer.PushLeft(item);
    }

    public T Dequeue()
    {
        ++_version;
        return _buffer.PopLeft();
    }

    public T DequeueRight()
    {
        ++_version;
        return _buffer.PopRight();
    }

    public bool TryDequeue(out T result)
    {
        bool succeeded = _buffer.TryPopLeft(out result);
        if (succeeded)
            ++_version;

        return succeeded;
    }

    public bool TryDequeueRight(out T result)
    {
        bool succeeded = _buffer.TryPopRight(out result);
        if (succeeded)
            ++_version;

        return succeeded;
    }

    public T Peek()
    {
        return _buffer.PeekLeft();
    }

    public T PeekRight()
    {
        return _buffer.PeekRight();
    }

    public bool TryPeek(out T result)
    {
        return _buffer.TryPeekLeft(out result);
    }

    public bool TryPeekRight(out T result)
    {
        return _buffer.TryPeekRight(out result);
    }

    public bool Contains(T item)
    {
        return _buffer.Contains(item);
    }

    public T[] ToArray()
    {
        return _buffer.ToArray();
    }

    public int EnsureCapacity(int capacity)
    {
        if (capacity < 0)
            throw new ArgumentOutOfRangeException(nameof(capacity));

        if (_buffer.Capacity < capacity)
        {
            Grow(capacity);
        }

        return _buffer.Capacity;
    }

    public void TrimExcess()
    {
        int threshold = (int)(_buffer.Capacity * 0.9);
        if (_buffer.Count < threshold)
        {
            ++_version;
            _buffer = new CircularList<T>(_buffer.Count, _buffer.ToArray());
        }
    }
    private void Grow(int capacity)
    {
        const int GrowFactor = 2;
        const int MinimumGrow = 2;

        int newCapacity = _buffer.Capacity * GrowFactor;
        if ((uint)newCapacity > Array.MaxLength)
            newCapacity = Array.MaxLength;

        newCapacity = Math.Max(newCapacity, _buffer.Capacity + MinimumGrow);

        if (newCapacity < capacity)
            newCapacity = capacity;

        ++_version;
        _buffer = new CircularList<T>(newCapacity, _buffer.ToArray());
    }

    public IEnumerator<T> GetEnumerator()
    {
        return new Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }

    public struct Enumerator : IEnumerator<T>
    {
        private readonly Deque<T> _deque;
        private readonly int _version;
        // -1 = not started. -2 = exhausted/disposed.
        private int _index;
        private T? _curentElement;

        internal Enumerator(Deque<T> d)
        {
            _deque = d;
            _version = d._version;
            _index = -1;
            _curentElement = default;
        }

        public void Dispose()
        {
            _index = -2;
            _curentElement = default;
        }

        public bool MoveNext()
        {
            if (_version != _deque._version)
                ThrowEnumerationVersionChanged();

            // Enumerator is exhausted.
            if (_index == -2)
                return false;

            ++_index;

            if (_index == _deque.Count)
            {
                _index = -2;
                _curentElement = default;
                return false;
            }

            _curentElement = _deque._buffer[_index];
            return true;
        }
        public T Current
        {
            get
            {
                if (_index < 0)
                    ThrowEnumerationNotStartedOrEnded();

                return _curentElement!;
            }
        }

        private void ThrowEnumerationNotStartedOrEnded()
        {
            throw new InvalidOperationException(_index == -1 ? "Enumerator not started." : "Enumerator has ended.");
        }

        private void ThrowEnumerationVersionChanged()
        {
            throw new InvalidOperationException("The CircularList has been modified.");
        }

        object? IEnumerator.Current
        {
            get { return Current; }
        }

        void IEnumerator.Reset()
        {
            if (_version != _deque._version)
                ThrowEnumerationVersionChanged();

            _index = -1;
            _curentElement = default;
        }
    }
}