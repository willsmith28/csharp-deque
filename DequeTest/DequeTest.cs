using Deque;

namespace DequeTest;

[TestClass]
public class DequeTest
{
    [TestMethod]
    public void EnqueueEmpty()
    {
        var deque = new Deque<int>();
        deque.Enqueue(0);
        Assert.AreEqual(0, deque.Dequeue());
    }
}