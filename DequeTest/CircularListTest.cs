using System.Linq;
using Deque;
namespace DequeTest;

[TestClass]
public class CircularListTest
{
    [TestMethod]
    public void TestEmptyConstructor()
    {
        int size = 16;
        var list = new CircularList<int>(size);
        Assert.AreEqual<int>(0, list.Count);
        Assert.AreEqual<int>(size, list.Capacity);
        Assert.IsTrue(list.IsEmpty);
        Assert.IsFalse(list.IsFull);
        Assert.IsTrue(Enumerable.SequenceEqual(list, new int[] { }));
    }

    [TestMethod]
    public void TestConstructor()
    {
        int size = 16;
        var inputList = new int[] { 0, 1, 2, 3, 4 };
        var list = new CircularList<int>(size, inputList);
        Assert.AreEqual<int>(inputList.Length, list.Count);
        Assert.AreEqual<int>(size, list.Capacity);
        Assert.IsFalse(list.IsEmpty);
        Assert.IsFalse(list.IsFull);
        Assert.IsTrue(Enumerable.SequenceEqual(list, inputList));

    }

    [TestMethod]
    public void TestConstructorInputLargerThanBuffer()
    {
        var list = new CircularList<int>(10, Enumerable.Range(0, 100));
        Assert.IsTrue(Enumerable.SequenceEqual(list, Enumerable.Range(90, 10)));
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void TestConstructorInvalidSize()
    {
        var list = new CircularList<int>(0);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
    public void TestConstructorNullArgument()
    {
        var list = new CircularList<int>(20, null!);
    }

    [TestMethod]
    public void TestEnumerator()
    {
        var list = new CircularList<int>(10, Enumerable.Range(0, 9));
        Assert.IsTrue(Enumerable.SequenceEqual(list, Enumerable.Range(0, 9)));
        list.PushRangeRight(Enumerable.Range(9, 6));
        Assert.IsTrue(Enumerable.SequenceEqual(list, Enumerable.Range(5, 10)));
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException))]
    public void TestPopLeftEmptyBuffer()
    {
        new CircularList<int>(1).PopLeft();
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException))]
    public void TestPopRightEmptyBuffer()
    {
        new CircularList<int>(1).PopRight();
    }

    [TestMethod]
    public void TestClear()
    {
        int size = 10;
        var list = new CircularList<int>(size, Enumerable.Range(1, 10));
        Assert.AreEqual<int>(size, list.Count);
        list.Clear();
        Assert.AreEqual<int>(0, list.Count);
        Assert.IsTrue(Enumerable.SequenceEqual(list, new int[] { }));
    }
}