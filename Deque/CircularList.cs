using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Deque;
public class CircularList<T> : IList<T>
{
    private readonly T[] _buffer;
    private int _head;
    private int _tail;
    private int _size;
    private int _version;
    public int Capacity { get { return _buffer.Length; } }
    public int Count { get { return _size; } }
    public bool IsEmpty { get { return _size == 0; } }
    public bool IsFull { get { return _size == _buffer.Length; } }
    public bool IsReadOnly { get { return false; } }

    public CircularList(int size) : this(size, new T[] { }) { }

    public CircularList(int size, T[] items)
    {
        if (size < 1)
            throw new ArgumentException("CircularList size must be greater than zero");

        if (items is null)
            throw new ArgumentNullException(nameof(items));

        _buffer = new T[size];
        _head = 0;
        _tail = 0;
        _size = 0;
        _version = 0;
        if (items.Length < size)
        {
            Array.Copy(items, 0, _buffer, 0, items.Length);
            _tail = items.Length;
            _size = items.Length;
        }
        else
        {
            Array.Copy(items, items.Length - size, _buffer, 0, size);
            _size = size;
        }
    }

    public CircularList(int size, IEnumerable<T> items)
    {
        if (size < 1)
            throw new ArgumentException("CircularList size must be greater than zero");

        if (items is null)
            throw new ArgumentNullException(nameof(items));

        _buffer = new T[size];
        _head = 0;
        _tail = 0;
        _size = 0;
        _version = 0;
        foreach (var item in items.Where(item => item is not null).TakeLast(size))
        {
            _buffer[_tail] = item;
            Increment(ref _tail);
            ++_size;
        }
    }

    public void Clear()
    {
        if (!IsEmpty)
        {
            if (_head < _tail)
            {
                Array.Clear(_buffer, _head, _size);
            }
            else
            {
                Array.Clear(_buffer, _head, _buffer.Length - _head);
                Array.Clear(_buffer, 0, _tail);
            }
            _size = 0;
        }
        ++_version;
        _head = 0;
        _tail = 0;
    }

    public bool Contains(T item)
    {
        if (IsEmpty)
        {
            return false;
        }
        if (_head < _tail)
        {
            return Array.IndexOf(_buffer, item, _head, _size) >= 0;
        }
        return
            Array.IndexOf(_buffer, item, _head, _buffer.Length - _head) >= 0 ||
            Array.IndexOf(_buffer, item, 0, _tail) >= 0;
    }

    // Add item to the end of the buffer. 
    // If buffer is full the first item will be overwritten.
    public void Add(T item)
    {
        PushRight(item);
    }

    public void PushRight(T item)
    {
        _buffer[_tail] = item;
        Increment(ref _tail);
        if (IsFull)
        {
            _head = _tail;
        }
        else
        {
            ++_size;
        }
        ++_version;
    }

    public void PushRangeRight(IEnumerable<T> items)
    {
        foreach (var item in items)
        {
            PushRight(item);
        }
    }

    // Add item to the front of the buffer.
    // if the buffer is full the last item will be overwritten
    public void PushLeft(T item)
    {
        Decrement(ref _head);
        _buffer[_head] = item;
        if (IsFull)
        {
            _tail = _head;
        }
        else
        {
            ++_size;
        }
        ++_version;
    }

    public void PushRangeLeft(IEnumerable<T> items)
    {
        foreach (var item in items)
        {
            PushLeft(item);
        }
    }


    // The elements that follow the insertion point move down to accommodate the new element.
    // If the List is at capacity then the last item will be removed.
    // If index equals the number of items in the CircularList, then item is pushed to the end of list, potentially removing the first item if the list is at capacity.
    public void Insert(int index, T newItem)
    {
        if (IsEmpty)
        {
            throw new IndexOutOfRangeException($"Index {index} out of range. Buffer is empty.");
        }
        if (index > _size)
        {
            throw new IndexOutOfRangeException($"Index {index} out of range. Buffer size {_size}.");
        }
        if (index == _size)
        {
            PushRight(newItem);
            return;
        }
        // Set new item to index specified.
        int current = RelativeIndex(index);
        T previousItem = _buffer[current];
        _buffer[current] = newItem;
        int next = current;
        Increment(ref next);
        // If this is not the last shift all items right.
        while (next != _tail)
        {
            T temp = _buffer[next];
            _buffer[next] = previousItem;
            previousItem = temp;
            Increment(ref next);
        }
        if (!IsFull)
        {
            _buffer[next] = previousItem;
            ++_size;
            Increment(ref _tail);
        }
        ++_version;
    }

    // Remove item from the end of the buffer.
    public T PopRight()
    {
        if (IsEmpty)
            ThrowForEmptyBuffer();

        Decrement(ref _tail);
        T item = _buffer[_tail];
        _buffer[_tail] = default!;
        --_size;
        ++_version;
        return item;

    }

    public bool TryPopRight(out T result)
    {
        if (IsEmpty)
        {
            result = default!;
            return false;
        }

        result = PopRight();
        return true;
    }

    // Remove item from front of the buffer.
    public T PopLeft()
    {
        if (IsEmpty)
            ThrowForEmptyBuffer();

        T item = _buffer[_head];
        _buffer[_head] = default!;
        Increment(ref _head);
        --_size;
        ++_version;
        return item;
    }

    public bool TryPopLeft(out T result)
    {
        if (IsEmpty)
        {
            result = default!;
            return false;
        }

        result = PopLeft();
        return true;
    }


    public void RemoveAt(int index)
    {
        if (IsEmpty)
        {
            throw new IndexOutOfRangeException($"Index {index} out of range. Buffer is empty.");
        }
        if (index >= _size)
        {
            throw new IndexOutOfRangeException($"Index {index} out of range. Buffer size {_size}.");
        }
        // Set item at index to default.
        int current = RelativeIndex(index);
        _buffer[current] = default!;
        // Move succeeding elements up one element.
        int next = index;
        Increment(ref next);
        while (next != _tail)
        {
            _buffer[current] = _buffer[next];
            _buffer[next] = default!;
            Increment(ref current);
            Increment(ref next);
        }
        Decrement(ref _tail);
        --_size;
        ++_version;
    }

    public bool Remove(T item)
    {
        int index = IndexOf(item);
        if (index == -1)
        {
            return false;
        }
        RemoveAt(index);
        return true;
    }

    // Returns item from front of the buffer without removing it.
    public T PeekLeft()
    {
        if (IsEmpty)
            ThrowForEmptyBuffer();

        return _buffer[_head];
    }

    public bool TryPeekLeft(out T result)
    {
        if (IsEmpty)
        {
            result = default!;
            return false;
        }
        result = _buffer[_head];
        return true;
    }

    // Returns item from end of the buffer without removing it.
    public T PeekRight()
    {
        if (IsEmpty)
            ThrowForEmptyBuffer();

        int temp = _tail;
        Decrement(ref temp);
        return _buffer[temp];
    }

    public bool TryPeekRight(out T result)
    {
        if (IsEmpty)
        {
            result = default!;
            return false;
        }
        int temp = _tail;
        Decrement(ref temp);
        result = _buffer[temp];
        return true;
    }

    public int IndexOf(T item)
    {
        if (IsEmpty)
        {
            return -1;
        }
        int result;
        Predicate<T> itemEquals = element => EqualityComparer<T>.Default.Equals(element, item);
        if (_head < _tail)
        {
            result = Array.FindIndex(_buffer, _head, _size, itemEquals);
        }
        else
        {
            result = Array.FindIndex(_buffer, _head, _buffer.Length - _head, itemEquals);
            if (result == -1)
            {
                result = Array.FindIndex(_buffer, 0, _tail, itemEquals);
            }
        }

        return result == -1 ? result : RelativeIndex(result);
    }

    public T this[int index]
    {
        get
        {
            if (IsEmpty)
            {
                throw new IndexOutOfRangeException($"Index {index} out of range. Buffer is empty.");
            }
            if (index >= _size)
            {
                throw new IndexOutOfRangeException($"Index {index} out of range. Buffer size {_size}.");
            }
            return _buffer[RelativeIndex(index)];
        }
        set
        {
            if (IsEmpty)
            {
                throw new IndexOutOfRangeException($"Index {index} out of range. Buffer is empty.");
            }
            if (index >= _size)
            {
                throw new IndexOutOfRangeException($"Index {index} out of range. Buffer size {_size}.");
            }
            ++_size;
            ++_version;
            _buffer[RelativeIndex(index)] = value;
        }
    }

    private void Increment(ref int index)
    {
        int temp = index + 1;
        if (temp == _buffer.Length)
        {
            temp = 0;
        }
        index = temp;
    }

    private void Decrement(ref int index)
    {
        int temp = index - 1;
        if (temp == -1)
        {
            temp = _buffer.Length - 1;
        }
        index = temp;
    }

    private int RelativeIndex(int index)
    {

        return (index < _buffer.Length - _head) ?
            _head + index :
            _head + index - _buffer.Length;
    }

    private void ThrowForEmptyBuffer()
    {
        throw new InvalidOperationException("Buffer empty.");
    }

    public void CopyTo(T[] array, int index)
    {
        ArgumentNullException.ThrowIfNull(array);
        if (index < 0 || index > array.Length)
            throw new ArgumentOutOfRangeException(nameof(index));

        if (array.Length - index < _size)
            throw new ArgumentException("CircularList is to large to fit in array provided.");

        foreach (var item in this)
        {
            array[index] = item;
            ++index;
        }
    }

    public T[] ToArray()
    {
        if (_size == 0)
        {
            return Array.Empty<T>();
        }

        T[] array = new T[_size];

        if (_head < _tail)
        {
            Array.Copy(_buffer, _head, array, 0, _size);
        }
        else
        {
            int headPartitionLength = _buffer.Length - _head;
            Array.Copy(_buffer, _head, array, 0, headPartitionLength);
            Array.Copy(_buffer, 0, array, headPartitionLength, _tail);
        }
        return array;
    }

    public IEnumerator<T> GetEnumerator()
    {
        return new Enumerator(this);
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return this.GetEnumerator();
    }


    public struct Enumerator : IEnumerator<T>
    {
        private readonly CircularList<T> _list;
        private readonly int _version;
        // -1 = not started. -2 = exhausted/disposed.
        private int _index;
        private T? _curentElement;

        internal Enumerator(CircularList<T> list)
        {
            _list = list;
            _version = list._version;
            _index = -1;
            _curentElement = default;
        }

        public void Dispose()
        {
            _index = -2;
            _curentElement = default;
        }

        public bool MoveNext()
        {
            if (_version != _list._version)
                ThrowEnumerationVersionChanged();

            // Enumerator is exhausted.
            if (_index == -2)
                return false;

            ++_index;

            if (_index == _list._size)
            {
                _index = -2;
                _curentElement = default;
                return false;
            }

            _curentElement = _list._buffer[_list.RelativeIndex(_index)];
            return true;
        }

        public T Current
        {
            get
            {
                if (_index < 0)
                    ThrowEnumerationNotStartedOrEnded();

                return _curentElement!;
            }
        }

        private void ThrowEnumerationNotStartedOrEnded()
        {
            throw new InvalidOperationException(_index == -1 ? "Enumerator not started." : "Enumerator has ended.");
        }

        private void ThrowEnumerationVersionChanged()
        {
            throw new InvalidOperationException("The CircularList has been modified.");
        }

        object? IEnumerator.Current
        {
            get { return Current; }
        }

        void IEnumerator.Reset()
        {
            if (_version != _list._version)
                ThrowEnumerationVersionChanged();

            _index = -1;
            _curentElement = default;
        }
    }
}
